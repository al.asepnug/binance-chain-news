package com.muxe.binancechainnews.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.databinding.FragmentDashboardBinding
import com.muxe.binancechainnews.entity.Categories
import com.muxe.binancechainnews.ui.binanceWebview.BinanceWebviewActivity
import com.muxe.binancechainnews.ui.listingCoin.ListingCoinActivity
import com.muxe.binancechainnews.ui.newsBscn.NewsBscnActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : Fragment() {
    private val binding by lazy { FragmentDashboardBinding.inflate(layoutInflater) }
    private val viewModel: DashboardViewModel by viewModels()
    private val listCategories = ArrayList<Categories>()
    private val listCategoriesNews = ArrayList<Categories>()
    private val listCategoriesTools = ArrayList<Categories>()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        initUi()
        populateCategories()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.appBarLayout.toolbar.logo = ContextCompat.getDrawable(
                requireActivity(),
                R.drawable.logo
        )
    }

    private fun initUi() {
        binding.rvCategories.apply {
            layoutManager = GridLayoutManager(context, 4)
            adapter = CategoriesAdapter(context, listCategories) {
                when (it.name) {
                    "Market Cap" -> {
                        val intent = Intent(context, ListingCoinActivity::class.java)
                        context?.startActivity(intent)
                    }
                    "Cryptoclopedia" -> {
                        val intent = Intent(context, BinanceWebviewActivity::class.java)
                        intent.putExtra(BinanceWebviewActivity.URL_NEWS, it.url)
                        intent.putExtra(NewsBscnActivity.TITLE_NEWS, it.name)
                        context?.startActivity(intent)
                    }
                    else -> {
                        val intent = Intent(context, NewsBscnActivity::class.java)
                        intent.putExtra(NewsBscnActivity.CATEGORIES_NEWS, it.id)
                        intent.putExtra(NewsBscnActivity.TITLE_NEWS, it.name)
                        context?.startActivity(intent)
                    }
                }
            }
        }

        binding.rvNewsMenu.apply {
            layoutManager = GridLayoutManager(context, 4)
            adapter = CategoriesAdapter(context, listCategoriesNews) {
                val intent = Intent(context, NewsBscnActivity::class.java)
                intent.putExtra(NewsBscnActivity.CATEGORIES_NEWS, it.id)
                intent.putExtra(NewsBscnActivity.TITLE_NEWS, it.name)
                context?.startActivity(intent)
            }
        }

        binding.rvToolMenu.apply {
            layoutManager = GridLayoutManager(context, 4)
            adapter = CategoriesAdapter(context, listCategoriesTools) {
                when (it.name) {
                    "BSC Explorer" -> {

                    }
                    "Cryptocloped" -> {
                        val intent = Intent(context, BinanceWebviewActivity::class.java)
                        intent.putExtra(BinanceWebviewActivity.URL_NEWS, it.url)
                        intent.putExtra(NewsBscnActivity.TITLE_NEWS, it.name)
                        context?.startActivity(intent)
                    }
                }
            }
        }
    }

    private fun populateCategories() {
        binding.rvCategories.adapter?.let { adapter ->
            when (adapter) {
                is CategoriesAdapter -> {
                    adapter.updateData(viewModel.getFrontPageCategories())
                }
            }
        }

        binding.rvNewsMenu.adapter?.let { adapter ->
            when (adapter) {
                is CategoriesAdapter -> {
                    adapter.updateData(viewModel.getNewsMenuCategories())
                }
            }
        }

        binding.rvToolMenu.adapter?.let { adapter ->
            when (adapter) {
                is CategoriesAdapter -> {
                    adapter.updateData(viewModel.getToolMenuCategories())
                }
            }
        }
    }
}