package com.muxe.binancechainnews.ui.notifications

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.data.repository.NewsRepository
import com.muxe.binancechainnews.entity.Posts
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationsViewModel @Inject constructor(
    private val newsRepository: NewsRepository
) : ViewModel() {

    private val _latestPostsResponse = MutableLiveData<ApiResponse<List<Posts>>>()

    val latestPostsResponse = _latestPostsResponse

    fun getLatestPosts(page: Int, perPage: Int) {
        viewModelScope.launch {
            newsRepository.getLatestPosts(page, perPage).collect{
                _latestPostsResponse.value = it
            }
        }
    }
}