package com.muxe.binancechainnews.ui.info

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.muxe.binancechainnews.BuildConfig
import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.databinding.FragmentInfoBinding
import com.muxe.binancechainnews.ui.binanceWebview.BinanceWebviewActivity
import com.muxe.binancechainnews.ui.contactUs.ContactUsActivity
import com.muxe.binancechainnews.ui.newsBscn.NewsBscnActivity

class InfoFragment : Fragment(), View.OnClickListener {
    private val binding by lazy { FragmentInfoBinding.inflate(layoutInflater) }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding.tvPublicity.setOnClickListener(this)
        binding.tvContentConsesus.setOnClickListener(this)
        binding.tvPrivacy.setOnClickListener(this)
        binding.tvTerms.setOnClickListener(this)
        binding.tvContact.setOnClickListener(this)
        binding.tvSendFeedback.setOnClickListener(this)
        binding.tvShareApps.setOnClickListener(this)
        binding.tvRatingApps.setOnClickListener(this)
        binding.tvVersionApps.text = "Version: ${BuildConfig.VERSION_NAME}"
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.appBarLayout.toolbar.logo = ContextCompat.getDrawable(
            requireActivity(),
            R.drawable.logo
        )
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tvPublicity -> {
                val intent = Intent(context, BinanceWebviewActivity::class.java)
                intent.putExtra(BinanceWebviewActivity.URL_NEWS, "https://binancechain.news/publicity-network/")
                intent.putExtra(NewsBscnActivity.TITLE_NEWS, "Publicity Network")
                context?.startActivity(intent)
            }

            R.id.tvContentConsesus -> {
                val intent = Intent(context, BinanceWebviewActivity::class.java)
                intent.putExtra(BinanceWebviewActivity.URL_NEWS, "https://binancechain.news/content-consensus/")
                intent.putExtra(NewsBscnActivity.TITLE_NEWS, "Contact")
                context?.startActivity(intent)
            }

            R.id.tvPrivacy -> {
                val intent = Intent(context, BinanceWebviewActivity::class.java)
                intent.putExtra(BinanceWebviewActivity.URL_NEWS, "https://binancechain.news/bscn-privacy-policy/")
                intent.putExtra(NewsBscnActivity.TITLE_NEWS, "Bscn Privacy Policy")
                context?.startActivity(intent)
            }

            R.id.tvTerms -> {
                val intent = Intent(context, BinanceWebviewActivity::class.java)
                intent.putExtra(BinanceWebviewActivity.URL_NEWS, "https://binancechain.news/bscn-terms-and-conditions/")
                intent.putExtra(NewsBscnActivity.TITLE_NEWS, "Bscn Terms and Conditions")
                context?.startActivity(intent)
            }

            R.id.tvContact -> {
                val intent = Intent(context, ContactUsActivity::class.java)
                context?.startActivity(intent)
            }

            R.id.tvSendFeedback -> {
                val email = Intent(Intent.ACTION_SEND)
                email.putExtra(Intent.EXTRA_EMAIL, arrayOf("business@binancechain.news"))
                email.putExtra(Intent.EXTRA_SUBJECT, "Contact Me")
                email.putExtra(Intent.EXTRA_TEXT, "empty")
                email.type = "message/rfc822"
                startActivity(Intent.createChooser(email, "Choose an Email client :"))
            }

            R.id.tvShareApps -> {
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, "Use apps binance chain, click here!! https://play.google.com/store/apps/details?id=com.muxe.binancechainnews")
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, "Doe mee met dit feest")
                startActivity(shareIntent)
            }

            R.id.tvRatingApps -> {
                val intent = Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse(
                        "https://play.google.com/store/apps/details?id=com.muxe.binancechainnews")
                    setPackage("com.android.vending")
                }
                startActivity(intent)
            }

        }
    }


}