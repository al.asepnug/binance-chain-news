package com.muxe.binancechainnews.ui.binanceWebview

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.muxe.binancechainnews.databinding.ActivityBinanceWebviewBinding
import com.muxe.binancechainnews.utils.makeToast
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class BinanceWebviewActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBinanceWebviewBinding
    private lateinit var url: String

    companion object {
        const val MAX_PROGRESS = 100
        const val TITLE_NEWS = "title_news"
        const val URL_NEWS = "url_news"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBinanceWebviewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        try {
            if (!intent.extras?.getString(TITLE_NEWS).isNullOrEmpty()) {
                supportActionBar?.title = intent.extras?.getString(TITLE_NEWS)
                url = intent.extras?.getString(URL_NEWS)!!
                initWebView()
                setWebClient()
                loadUrl(url)
            }
        } catch (e: IOException) {
            makeToast("Error Load Data")
        }

    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        val settings = binding.webView.settings

        // Enable java script in web view
        settings.javaScriptEnabled = true

        // Enable and setup web view cache
        settings.setAppCacheEnabled(true)
        settings.cacheMode = WebSettings.LOAD_DEFAULT
        settings.setAppCachePath(cacheDir.path)


        // Enable zooming in web view
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true
        settings.displayZoomControls = true
        // Zoom web view text
        settings.textZoom = 125

        // Enable disable images in web view
        settings.blockNetworkImage = false
        // Whether the WebView should load image resources
        settings.loadsImagesAutomatically = true

        // More web view settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            settings.safeBrowsingEnabled = true  // api 26
        }
        //settings.pluginState = WebSettings.PluginState.ON
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.mediaPlaybackRequiresUserGesture = false

        // More optional settings, you can enable it by yourself
        settings.domStorageEnabled = true
        settings.setSupportMultipleWindows(true)
        settings.loadWithOverviewMode = true
        settings.allowContentAccess = true
        settings.setGeolocationEnabled(true)
        settings.allowUniversalAccessFromFileURLs = true
        settings.allowFileAccess = true

        // WebView settings
        binding.webView.fitsSystemWindows = true


        /*
            if SDK version is greater of 19 then activate hardware acceleration
            otherwise activate software acceleration
        */
        binding.webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)

    }

    private fun setWebClient() {
        binding.webView.webViewClient = object: WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {
                view.loadUrl(url!!)
                return false
            }

            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                // Page loading started
                // Do something
                binding.progressBar.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView, url: String) {
                // Page loading finished
                // Display the loaded page title in a toast message
                binding.progressBar.visibility = View.GONE
                if (url != "https://binancechain.news/") {
                    binding.webView.loadUrl("javascript:document.getElementById(\"header\").setAttribute(\"style\",\"display:none;\");")
                    binding.webView.loadUrl("javascript:document.getElementById(\"footer\").setAttribute(\"style\",\"display:none;\");")
                }
            }
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && binding.webView.canGoBack()) {
            binding.webView.goBack()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun loadUrl(pageUrl: String?) {
        binding.webView.loadUrl(pageUrl!!)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}