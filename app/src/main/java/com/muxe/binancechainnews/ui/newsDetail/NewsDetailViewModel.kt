package com.muxe.binancechainnews.ui.newsDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.muxe.binancechainnews.data.local.entity.Post
import com.muxe.binancechainnews.data.repository.NewsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NewsDetailViewModel @Inject constructor(
        private val newsRepository: NewsRepository
) : ViewModel() {

    fun setFavoritePost(post: Post) { newsRepository.insertPostFavorite(post) }

    fun deleteFavoritePost(post: Post) { newsRepository.deletePostFavorite(post) }

    fun getPostIsFavorite(id: Int?): LiveData<Post> = newsRepository.getPostIsFavorite(id)

}

