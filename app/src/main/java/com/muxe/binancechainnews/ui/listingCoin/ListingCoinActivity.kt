package com.muxe.binancechainnews.ui.listingCoin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.databinding.ActivityListingCoinBinding
import com.muxe.binancechainnews.entity.Coin
import com.muxe.binancechainnews.utils.makeToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListingCoinActivity : AppCompatActivity() {
    private lateinit var binding: ActivityListingCoinBinding
    private val listCoin = ArrayList<Coin>()
    private val viewModel: ListingViewModel by viewModels()
    private var isLoading: Boolean = false
    private var limit: Int = 10
    private var start: Int = 1
    private var isLastPage: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListingCoinBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Crypto Market Cap"
        initUi()
        populateListingCoin()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun initUi() {
        binding.rvCoin.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = ListingCoinAdapter(context, listCoin) {
                //showMessage(it.toString())
            }
        }

        binding.rvCoin.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition
                if (!isLoading && isLastPosition && !isLastPage) {
                    start = start.plus(10)
                    if (start == 1) {
                        binding.loadBar.visibility = View.VISIBLE
                    } else {
                        showLazyLoading(true)
                    }
                    viewModel.getListingCoin(start, limit)
                }
            }
        })
    }

    private fun populateListingCoin() {
        viewModel.listingCoinResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    if (start == 1) {
                        binding.loadBar.visibility = View.GONE
                    } else {
                        showLazyLoading(false)
                    }
                    binding.loadBar.visibility = View.GONE
                    result.body?.let {
                        binding.rvCoin.adapter?.let { adapter ->
                            when (adapter) {
                                is ListingCoinAdapter -> {
                                    adapter.updateData(it.data!!)
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    if (start == 1) {
                        binding.loadBar.visibility = View.GONE
                    } else {
                        start.minus(10)
                        showLazyLoading(false)
                    }
                    result.message?.let { makeToast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }

    private fun showLazyLoading(isRefresh: Boolean) {
        if (isRefresh) {
            binding.lazyLoadBar.visibility = View.VISIBLE
            isLoading = true
        } else {
            binding.lazyLoadBar.visibility = View.INVISIBLE
            isLoading = false
        }
    }
}