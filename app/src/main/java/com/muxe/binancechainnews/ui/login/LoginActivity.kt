package com.muxe.binancechainnews.ui.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.utils.makeStatusBarTransparent

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }
}