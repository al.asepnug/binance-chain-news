package com.muxe.binancechainnews.ui.newsFavorite

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.binancechainnews.data.local.entity.Post
import com.muxe.binancechainnews.databinding.ItemNewsBinding
import com.muxe.binancechainnews.utils.ValueFormat
import com.muxe.binancechainnews.utils.showImageCenterInside

class NewsFavoriteAdapter (private val context: Context, private val postListItem: ArrayList<Post>, private val clickListener: (Post) -> Unit)
    : RecyclerView.Adapter<NewsFavoriteAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemNewsBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return postListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(postListItem[position])
    }

    inner class ViewHolder(private val binding: ItemNewsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Post) {
            binding.tvTitle.text = data.title
            binding.tvDate.text = ValueFormat.formatDate(data.date)
            data.linkMedia?.let { context.showImageCenterInside(it, binding.imgNews) }
            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun updateData(newList: List<Post>) {
        postListItem.clear()
        postListItem.addAll(newList)
        notifyDataSetChanged()
    }
}