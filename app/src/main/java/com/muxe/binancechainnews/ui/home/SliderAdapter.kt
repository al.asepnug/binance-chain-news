package com.muxe.binancechainnews.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.muxe.binancechainnews.databinding.ItemSliderBinding
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.utils.ValueFormat.formatDate

class SliderAdapter(private val postsListItem: ArrayList<Posts>, private val clickListener: (Posts) -> Unit)
    : RecyclerView.Adapter<SliderAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemSliderBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return postsListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(postsListItem[position])
    }

    inner class ViewHolder(private val binding: ItemSliderBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Posts) {
            binding.tvTitleNews.text = data.title?.rendered
            binding.tvDateNews.text = formatDate(data.date)
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
            Glide.with(itemView)
                .load(data.embedded?.wpFeaturedmedia?.get(0)?.link)
                .apply(requestOptions)
                .into(binding.bannerImageView)

            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun updateData(newList: List<Posts>) {
        postsListItem.clear()
        postsListItem.addAll(newList)
        notifyDataSetChanged()
    }
}