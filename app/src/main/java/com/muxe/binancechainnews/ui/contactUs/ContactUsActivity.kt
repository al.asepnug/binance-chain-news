package com.muxe.binancechainnews.ui.contactUs

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.databinding.ActivityContactUsBinding

class ContactUsActivity : AppCompatActivity(), View.OnClickListener  {
    private lateinit var binding: ActivityContactUsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContactUsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "Contact Us"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvPhoneVal.setOnClickListener(this)
        binding.tvEmailVal.setOnClickListener(this)
        binding.tvWebsiteVal.setOnClickListener(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tvPhoneVal -> {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + binding.tvPhoneVal.text)
                startActivity(callIntent)
            }

            R.id.tvEmailVal -> {
                val email = Intent(Intent.ACTION_SEND)
                email.putExtra(Intent.EXTRA_EMAIL, arrayOf(binding.tvEmailVal.text as String))
                email.putExtra(Intent.EXTRA_SUBJECT, "Contact Me")
                email.putExtra(Intent.EXTRA_TEXT, "empty")
                email.type = "message/rfc822"
                startActivity(Intent.createChooser(email, "Choose an Email client :"))
            }

            R.id.tvWebsiteVal -> {
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(binding.tvWebsiteVal.text as String)
                startActivity(i)
            }
        }
    }
}