package com.muxe.binancechainnews.ui.newsBscn

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.binancechainnews.databinding.ItemFilterBinding
import com.muxe.binancechainnews.entity.Author
import com.muxe.binancechainnews.utils.DataDummy
import com.muxe.binancechainnews.utils.showImageCircleCrop

class FilterAdapter (private val context: Context, private val postsListItem: ArrayList<Author>, private val clickListener: (Author) -> Unit)
    : RecyclerView.Adapter<FilterAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemFilterBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return postsListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(postsListItem[position])
    }

    inner class ViewHolder(private val binding: ItemFilterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Author) {
            binding.tvAuthor.text = data.name
            val findedAvatarDummy = DataDummy.generateDataAvatarDummy().find {
                it.id == data.id
            }
            if (findedAvatarDummy != null) {
                context.showImageCircleCrop(findedAvatarDummy.imgUrl, binding.imageView)
            } else {
                data.avatarUrls?.jsonMember96?.let { context.showImageCircleCrop(it, binding.imageView) }
            }

            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun updateData(newList: List<Author>) {
        postsListItem.clear()
        postsListItem.addAll(newList)
        notifyDataSetChanged()
    }
}