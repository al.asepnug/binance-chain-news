package com.muxe.binancechainnews.ui.dashboard

import androidx.lifecycle.ViewModel
import com.muxe.binancechainnews.data.repository.NewsRepository
import com.muxe.binancechainnews.entity.Categories
import com.muxe.binancechainnews.utils.DataDummy
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(
        private val newsRepository: NewsRepository
) : ViewModel() {

    fun getFrontPageCategories(): List<Categories> = DataDummy.generateDataCategoriesDummy()

    fun getNewsMenuCategories(): List<Categories> = DataDummy.generateDataNewsMenuDummy()

    fun getToolMenuCategories(): List<Categories> = DataDummy.generateDataToolMenuDummy()

}