package com.muxe.binancechainnews.ui.notifications

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.databinding.FragmentNotificationsBinding
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.ui.home.NewsAdapter
import com.muxe.binancechainnews.ui.newsDetail.NewsDetailActivity
import com.muxe.binancechainnews.utils.makeToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationsFragment : Fragment() {
    private val binding by lazy { FragmentNotificationsBinding.inflate(layoutInflater) }
    private val viewModel: NotificationsViewModel by viewModels()
    private val listPosts = ArrayList<Posts>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initUi()
        viewModel.getLatestPosts(1, 9)
        populateData()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.appBarLayout.toolbar.logo = ContextCompat.getDrawable(
            requireActivity(),
            R.drawable.logo
        )
    }

    private fun initUi() {
        binding.rvNotification.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = NotificationsAdapter(context, listPosts) {
                val intent = Intent(context, NewsDetailActivity::class.java)
                intent.putExtra(NewsDetailActivity.TYPE_NEWS, "news_latest")
                intent.putExtra(NewsDetailActivity.DATA, it)
                context.startActivity(intent)
            }
        }
    }

    private fun populateData() {
        viewModel.latestPostsResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.rvNotification.adapter?.let { adapter ->
                            when (adapter) {
                                is NotificationsAdapter -> {
                                    adapter.updateData(it)
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let {
                        requireActivity().makeToast(it)
                    }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }


}