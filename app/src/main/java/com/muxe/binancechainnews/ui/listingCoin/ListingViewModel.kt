package com.muxe.binancechainnews.ui.listingCoin

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.data.repository.NewsRepository
import com.muxe.binancechainnews.entity.ListingCoin
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListingViewModel @Inject constructor(
        private val newsRepository: NewsRepository
) : ViewModel() {
    private val _listingCoinResponse = MutableLiveData<ApiResponse<ListingCoin>>()

    val listingCoinResponse = _listingCoinResponse

    fun getListingCoin(start: Int, limit: Int) {
        viewModelScope.launch {
            newsRepository.getListingCoin(start, limit).collect{
                _listingCoinResponse.value = it
            }
        }
    }

}