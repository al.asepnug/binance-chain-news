package com.muxe.binancechainnews.ui.notifications

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.binancechainnews.databinding.ItemNotificationsBinding
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.utils.showImageCenterInside

class NotificationsAdapter (private val context: Context, private val postsListItem: ArrayList<Posts>,
                            private val clickListener: (Posts) -> Unit)
    : RecyclerView.Adapter<NotificationsAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemNotificationsBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return postsListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(postsListItem[position])
    }

    inner class ViewHolder(private val binding: ItemNotificationsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Posts) {
            binding.tvTitle.text = data.title?.rendered
            data.embedded?.wpFeaturedmedia?.get(0)?.link?.let { context.showImageCenterInside(it, binding.imgNews) }

            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun updateData(newList: List<Posts>) {
        postsListItem.clear()
        postsListItem.addAll(newList)
        notifyDataSetChanged()
    }
}