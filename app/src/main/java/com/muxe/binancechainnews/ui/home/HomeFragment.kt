package com.muxe.binancechainnews.ui.home

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayoutMediator
import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.databinding.FragmentHomeBinding
import com.muxe.binancechainnews.entity.Categories
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.ui.binanceWebview.BinanceWebviewActivity
import com.muxe.binancechainnews.ui.binanceWebview.BinanceWebviewActivity.Companion.URL_NEWS
import com.muxe.binancechainnews.ui.contactUs.ContactUsActivity
import com.muxe.binancechainnews.ui.listingCoin.ListingCoinActivity
import com.muxe.binancechainnews.ui.newsBscn.NewsBscnActivity
import com.muxe.binancechainnews.ui.newsBscn.NewsBscnActivity.Companion.CATEGORIES_NEWS
import com.muxe.binancechainnews.ui.newsBscn.NewsBscnActivity.Companion.TITLE_NEWS
import com.muxe.binancechainnews.ui.newsDetail.NewsDetailActivity
import com.muxe.binancechainnews.ui.newsFavorite.NewsFavoriteActivity
import com.muxe.binancechainnews.utils.DebouncingQueryTextListener
import com.muxe.binancechainnews.utils.makeToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment(), View.OnClickListener {
    private val binding by lazy { FragmentHomeBinding.inflate(layoutInflater) }
    private val viewModel: HomeViewModel by viewModels()
    private val listPosts = ArrayList<Posts>()
    private val listCategories = ArrayList<Categories>()
    private var isLoading: Boolean = false
    private var page: Int = 1
    private var isLastPage: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding.viewLatestNewsPost.setOnClickListener(this)
        binding.viewLatestPost.setOnClickListener(this)
        setHasOptionsMenu(true)
        initUi()
        viewModel.getLatestPosts(page, 9)
        populateData()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.appBarLayout.toolbar.logo = ContextCompat.getDrawable(
            requireActivity(),
            R.drawable.logo
        )
        binding.appBarLayout.toolbar.inflateMenu(R.menu.main_menu)
        binding.appBarLayout.toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_favorite -> {
                    val intent = Intent(context, NewsFavoriteActivity::class.java)
                    startActivity(intent)
                    true
                }

                else -> false
            }
        }

        val searchItem: MenuItem = binding.appBarLayout.toolbar.menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as androidx.appcompat.widget.SearchView
        searchView.setOnCloseListener { true }
        val searchPlate = searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
        searchPlate.hint = "Search News"
        val searchPlateView: View =
            searchView.findViewById(androidx.appcompat.R.id.search_plate)
        searchPlateView.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                android.R.color.transparent
            )
        )

        searchView.setOnQueryTextListener(DebouncingQueryTextListener(
            requireActivity()
        ) { newText ->
            newText?.let {
                if (it.isEmpty()) {
                    binding.searchContent.visibility = View.GONE
                    binding.contentLay.visibility = View.VISIBLE
                } else {
                    binding.contentLay.visibility = View.INVISIBLE
                    binding.searchContent.visibility = View.VISIBLE
                    binding.loadBar.visibility = View.VISIBLE
                    viewModel.getSearchPosts(it)
                }
            }
        })

        searchView.setOnCloseListener {
            searchView.onActionViewCollapsed()
            false
        }
        val searchManager = requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
    }

    private fun initUi() {
        binding.rvLatestNews.apply {
            layoutManager = GridLayoutManager(context, 3)
            adapter = PostsAdapter(context, listPosts) {
                val intent = Intent(context, NewsDetailActivity::class.java)
                intent.putExtra(NewsDetailActivity.TYPE_NEWS, "news_latest")
                intent.putExtra(NewsDetailActivity.DATA, it)
                context.startActivity(intent)
            }
        }

        binding.banner.apply {
            adapter = SliderAdapter(listPosts) {
                val intent = Intent(context, NewsDetailActivity::class.java)
                intent.putExtra(NewsDetailActivity.TYPE_NEWS, "news_latest")
                intent.putExtra(NewsDetailActivity.DATA, it)
                context.startActivity(intent)
            }
        }

        binding.rvNews.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = NewsAdapter(context, listPosts) {
                val intent = Intent(context, NewsDetailActivity::class.java)
                intent.putExtra(NewsDetailActivity.TYPE_NEWS, "news_latest")
                intent.putExtra(NewsDetailActivity.DATA, it)
                context.startActivity(intent)
            }
        }

        //binding.banner.offscreenPageLimit = 3
        binding.banner.clipToPadding = true
        //binding.banner.clipChildren = false
        TabLayoutMediator(binding.indicator, binding.banner)
        { tab, position -> }.attach()

        binding.rvCategories.apply {
            layoutManager = GridLayoutManager(context, 4)
            adapter = CategoriesAdapter(context, listCategories) {
                when (it.name) {
                    "Market Cap" -> {
                        val intent = Intent(context, ListingCoinActivity::class.java)
                        context?.startActivity(intent)
                    }
                    "Cryptoclopedia" -> {
                        val intent = Intent(context, BinanceWebviewActivity::class.java)
                        intent.putExtra(URL_NEWS, it.url)
                        intent.putExtra(TITLE_NEWS, it.name)
                        context?.startActivity(intent)
                    }
                    else -> {
                        val intent = Intent(context, NewsBscnActivity::class.java)
                        intent.putExtra(CATEGORIES_NEWS, it.id)
                        intent.putExtra(TITLE_NEWS, it.name)
                        context?.startActivity(intent)
                    }
                }
            }
        }

        binding.rvCategories.adapter?.let { adapter ->
            when (adapter) {
                is CategoriesAdapter -> {
                    Log.d("categoriesadapter", viewModel.getListCategories().size.toString())
                    adapter.updateData(viewModel.getListCategories())
                }
            }
        }

        binding.rvLatestNews.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val gridLayoutManager = recyclerView.layoutManager as GridLayoutManager
                val countItem = gridLayoutManager.itemCount
                val lastVisiblePosition = gridLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition
                if (!isLoading && isLastPosition && !isLastPage) {
                    page = page.plus(1)
                    if (page == 1) {
                        binding.progressBar.visibility = View.VISIBLE
                        //binding.progressBarBanner.visibility = View.VISIBLE

                    } else {
                        showLazyLoading(true)
                    }
                    viewModel.getLatestPosts(page, 9)
                }
            }
        })
    }

    private fun showLazyLoading(isRefresh: Boolean) {
        if (isRefresh) {
            binding.lazyLoadBar.visibility = View.VISIBLE
            isLoading = true
        } else {
            binding.lazyLoadBar.visibility = View.INVISIBLE
            isLoading = false
        }
    }

    private fun populateData() {
        viewModel.latestPostsResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    if (page == 1) {
                        binding.progressBar.visibility = View.GONE
                    } else {
                        showLazyLoading(false)
                    }
                    result.body?.let {
                        binding.rvLatestNews.adapter?.let { adapter ->
                            when (adapter) {
                                is PostsAdapter -> {
                                    adapter.updateData(it)
                                }
                            }
                        }

                        if (page == 1) {
                            binding.banner.adapter?.let { adapter ->
                                when (adapter) {
                                    is SliderAdapter -> {
                                        adapter.updateData(it)
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let {
                        if (page == 1) {
                            binding.progressBar.visibility = View.GONE
                        } else {
                            if (it != "400") {
                                requireActivity().makeToast(it)
                                page.minus(1)
                            } else {
                                requireActivity().makeToast("Last Data")
                            }
                            showLazyLoading(false)
                        }
                    }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })

        viewModel.searchPostsResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.loadBar.visibility = View.GONE
                        binding.rvNews.adapter?.let { adapter ->
                            when (adapter) {
                                is NewsAdapter -> {
                                    adapter.updateData(it)
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    binding.loadBar.visibility = View.GONE
                    binding.searchContent.visibility = View.GONE
                    binding.contentLay.visibility = View.VISIBLE
                    result.message?.let { requireActivity().makeToast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.viewLatestNewsPost -> {
                val intent = Intent(context, NewsBscnActivity::class.java)
                intent.putExtra(CATEGORIES_NEWS, "1")
                intent.putExtra(TITLE_NEWS, "BSCN News")
                context?.startActivity(intent)
            }

            R.id.viewLatestPost -> {
                val intent = Intent(context, BinanceWebviewActivity::class.java)
                intent.putExtra(URL_NEWS, "https://binancechain.news/")
                intent.putExtra(TITLE_NEWS, "Binancechain.news Webviewer")
                context?.startActivity(intent)
            }
        }
    }

}