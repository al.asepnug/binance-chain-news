package com.muxe.binancechainnews.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.binancechainnews.databinding.ItemCatagoriesBinding
import com.muxe.binancechainnews.entity.Categories
import com.muxe.binancechainnews.utils.showImageCenterInside

class CategoriesAdapter(private val context: Context,
                        private val categoriesListItem: ArrayList<Categories>,
                        private val clickListener: (Categories) -> Unit
) : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemCatagoriesBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
        )

        return ViewHolder(
                binding
        )
    }

    override fun getItemCount(): Int {
        return categoriesListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(categoriesListItem[position])
    }

    inner class ViewHolder(private val binding: ItemCatagoriesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Categories) {
            if ((bindingAdapterPosition+1)%4 == 0) {
                binding.view2.visibility = View.INVISIBLE
            }

            if (bindingAdapterPosition == (categoriesListItem.size-1) ||
                    bindingAdapterPosition == (categoriesListItem.size-2) ||
                    bindingAdapterPosition == (categoriesListItem.size-3) ||
                    bindingAdapterPosition == (categoriesListItem.size-4)) {
                        binding.view1.visibility = View.INVISIBLE
            }
            binding.tvNameMenu.text = data.name
            context.showImageCenterInside(data.image, binding.imgCategories)

            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun updateData(newList: List<Categories>) {
        categoriesListItem.clear()
        categoriesListItem.addAll(newList)
        notifyDataSetChanged()
    }
}