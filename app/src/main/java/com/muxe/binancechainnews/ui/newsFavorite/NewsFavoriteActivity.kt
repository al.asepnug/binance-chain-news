package com.muxe.binancechainnews.ui.newsFavorite

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.muxe.binancechainnews.data.local.entity.Post
import com.muxe.binancechainnews.databinding.ActivityNewsFavoriteBinding
import com.muxe.binancechainnews.ui.newsDetail.NewsDetailActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewsFavoriteActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNewsFavoriteBinding
    private val viewModel: NewsFavoriteViewModel by viewModels()
    private val listPost = ArrayList<Post>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewsFavoriteBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "Favorite News"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initUi()
        populateFavorite()
    }

    private fun initUi() {
        binding.rvNews.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = NewsFavoriteAdapter(context, listPost) {
                val intent = Intent(context, NewsDetailActivity::class.java)
                intent.putExtra(NewsDetailActivity.TYPE_NEWS, "news_favorite")
                intent.putExtra(NewsDetailActivity.DATA, it)
                context.startActivity(intent)
            }
        }
    }

    private fun populateFavorite() {
        viewModel.getPostFavorite().observe(this, { result ->
            binding.rvNews.adapter?.let { adapter ->
                when (adapter) {
                    is NewsFavoriteAdapter -> {
                        adapter.updateData(result)
                    }
                }
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}