package com.muxe.binancechainnews.ui.hunt

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.databinding.FragmentHuntBinding
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.entity.RefCountryCode
import com.muxe.binancechainnews.ui.newsDetail.NewsDetailActivity
import com.muxe.binancechainnews.utils.CustomInfoWindowMarker
import com.muxe.binancechainnews.utils.ValueFormat.bitmapDescriptorFromVector
import com.muxe.binancechainnews.utils.ValueFormat.getJsonDataFromAsset
import com.muxe.binancechainnews.utils.makeToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import java.util.*

@AndroidEntryPoint
class HuntFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private val binding by lazy { FragmentHuntBinding.inflate(layoutInflater) }
    private val viewModel: HuntViewModel by viewModels()
    private lateinit var mMap: GoogleMap
    private var myMarkers = ArrayList<Marker?>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // init ui

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)
    }

    @SuppressLint("PotentialBehaviorOverride")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true
        val customInfoWindow = CustomInfoWindowMarker(requireContext())
        mMap.setInfoWindowAdapter(customInfoWindow)
        mMap.setOnInfoWindowClickListener {
            val data: Posts = it.tag as Posts
            val intent = Intent(context, NewsDetailActivity::class.java)
            intent.putExtra(NewsDetailActivity.TYPE_NEWS, "news_latest")
            intent.putExtra(NewsDetailActivity.DATA, data)
            context?.startActivity(intent)
        }
//        mMap.setOnCameraMoveListener {
//            val cameraPosition = googleMap.cameraPosition
//            Log.d("zoomLevel", cameraPosition.zoom.toString())
//            Log.d("myMarkersize", myMarkers.size.toString())
//            if (cameraPosition.zoom > 10.0) {
//                Log.d("isPosition", ">10")
//                GlobalScope.launch(Dispatchers.Main) {
//                    for (m in myMarkers) {
//                        m?.showInfoWindow()
//                        Log.d("more15", m.toString())
//                    }
//                }
//            } else {
//                Log.d("isPosition", "<10")
//                GlobalScope.launch(Dispatchers.Main) {
//                    for (m in myMarkers) {
//                        m?.hideInfoWindow()
//                        Log.d("above15", m.toString())
//                    }
//                }
//            }
//        }

        populateDataPosts()
        viewModel.getLatestPosts(1, 100)
        countryLocationBound()
    }

    override fun onMarkerClick(p0: Marker) = false

    private fun countryLocationBound() {
        val jsonFileString = getJsonDataFromAsset(requireContext(), "RefCountryCodes.json")
        if (jsonFileString != null) {
            val jsonObjek = JSONObject(jsonFileString)
            val iterator: Iterator<String> = jsonObjek.keys()
            while (iterator.hasNext()) {
                val key = iterator.next()
                try {
                    if (Locale.getDefault().isO3Country == key) {
                        //if ("NLD" == key) {
                        val gson = Gson()
                        val refCountryCodesType = object : TypeToken<RefCountryCode>() {}.type
                        val refCountryCodes: RefCountryCode = gson.fromJson(
                            jsonObjek.optString(key),
                            refCountryCodesType
                        )
                        val countryBound = LatLngBounds(
                            LatLng(
                                (refCountryCodes.sw?.lat!!),
                                refCountryCodes.sw.lon!!
                            ),  // SW bounds
                            LatLng((refCountryCodes.ne?.lat!!), refCountryCodes.ne.lon!!)
                        )

                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(countryBound, 0))
                        mMap.setLatLngBoundsForCameraTarget(countryBound)
                        break
                    }
                } catch (e: JSONException) {
                }
            }
        }
    }

    private fun populateDataPosts() {
//        viewModel.getLatestPosts(1, 100).observe(viewLifecycleOwner, { result ->
        viewModel.latestPostsResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let { it ->
                        GlobalScope.launch(Dispatchers.Main) {
                            it.forEach { data ->
                                if (!data.latitude.equals("") && !data.longitude.equals("")) {
                                    val marker: Marker? = mMap.addMarker(
                                        MarkerOptions().position(
                                            LatLng(
                                                data.latitude!!.toDouble(),
                                                data.longitude!!.toDouble()
                                            )
                                        ).title(data.id.toString())
                                            .icon(
                                                bitmapDescriptorFromVector(
                                                    requireContext(),
                                                    R.drawable.ic_favorite
                                                )
                                            )
                                    )
                                    marker?.tag = data
                                    marker?.showInfoWindow()
                                    myMarkers.add(marker)
                                }
                            }

                        }

                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { requireActivity().makeToast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }
}