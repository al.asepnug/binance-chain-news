package com.muxe.binancechainnews.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.data.repository.NewsRepository
import com.muxe.binancechainnews.entity.Categories
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.utils.DataDummy
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlinx.coroutines.flow.collect

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val newsRepository: NewsRepository
) : ViewModel() {

    private val _latestPostsResponse = MutableLiveData<ApiResponse<List<Posts>>>()
    private val _searchPostsResponse = MutableLiveData<ApiResponse<List<Posts>>>()

    val latestPostsResponse = _latestPostsResponse
    val searchPostsResponse = _searchPostsResponse

    fun getLatestPosts(page: Int, perPage: Int) {
        viewModelScope.launch {
            newsRepository.getLatestPosts(page, perPage).collect{
                _latestPostsResponse.value = it
            }
        }
    }

    fun getSearchPosts(search: String) {
        viewModelScope.launch {
            newsRepository.getSearchPosts(search).collect{
                _latestPostsResponse.value = it
            }
        }
    }

    fun getListCategories(): List<Categories> = DataDummy.generateDataCategoriesDummy()

}