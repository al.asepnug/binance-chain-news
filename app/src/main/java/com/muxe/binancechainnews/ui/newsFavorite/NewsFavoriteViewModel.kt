package com.muxe.binancechainnews.ui.newsFavorite

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.muxe.binancechainnews.data.local.entity.Post
import com.muxe.binancechainnews.data.repository.NewsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NewsFavoriteViewModel @Inject constructor(
    private val newsRepository: NewsRepository
) : ViewModel() {
    fun getPostFavorite(): LiveData<List<Post>> = newsRepository.getPostsFavorite()
}