package com.muxe.binancechainnews.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.muxe.binancechainnews.R

class SplashActivity : AppCompatActivity() {
    lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        try {
            Firebase.messaging.subscribeToTopic("all")
                .addOnCompleteListener { task ->
//                    if (!task.isSuccessful) {
//                        Log.d("firebase", "subscribed_failed")
//                    } else {
//                        Log.d("firebase", "subscribed_successful")
//                    }
                }
        } catch (exception: Exception) {
            //Log.d("firebase", exception.toString())
        }

        handler = Handler()
        handler.postDelayed({
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 500)
    }
}