package com.muxe.binancechainnews.ui.newsBscn

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.databinding.ActivityNewsBscnBinding
import com.muxe.binancechainnews.entity.Author
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.ui.newsDetail.NewsDetailActivity
import com.muxe.binancechainnews.utils.makeToast
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class NewsBscnActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNewsBscnBinding
    private val listPosts = ArrayList<Posts>()
    private val listAuthor = ArrayList<Author>()
    private val viewModel: NewsBscnViewModel by viewModels()
    private lateinit var categories: String
    private var isLoading: Boolean = false
    private var page: Int = 1
    private var isLastPage: Boolean = false
    private var author: Int = 0

    companion object {
        const val TITLE_NEWS = "title_news"
        const val CATEGORIES_NEWS = "categories_news"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewsBscnBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        try {
            if (!intent.extras?.getString(CATEGORIES_NEWS).isNullOrEmpty()) {
                supportActionBar?.title = intent.extras?.getString(TITLE_NEWS)
                categories = intent.extras?.getString(CATEGORIES_NEWS)!!
                initUi()
                populateData()
                viewModel.getAuthor()
                populateBscnPosts(categories, page)
            }
        } catch (e: IOException) {
            makeToast("Error Load Data")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun initUi() {
        binding.loadBarFilter.visibility = View.VISIBLE

        binding.rvNews.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = NewsBscnAdapter(context, listPosts) {
                val intent = Intent(context, NewsDetailActivity::class.java)
                intent.putExtra(NewsDetailActivity.TYPE_NEWS, "news_latest")
                intent.putExtra(NewsDetailActivity.DATA, it)
                context.startActivity(intent)
            }
        }

        binding.rvFilter.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = FilterAdapter(context, listAuthor) {
                if (it.id != null) {
                    page = 1
                    author = it.id
                    Log.d("authoree", author.toString())
                    populateFilterBscnPosts(author, page)
                }
            }
        }

        binding.rvNews.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition =
                    linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition
                if (!isLoading && isLastPosition && !isLastPage) {
                    page = page.plus(1)
                    if (author == 0) {
                        populateBscnPosts(categories, page)
                    } else {
                        populateFilterBscnPosts(author, page)
                    }
                }
            }
        })
    }

    private fun showLazyLoading(isRefresh: Boolean) {
        if (isRefresh) {
            binding.lazyLoadBar.visibility = View.VISIBLE
            isLoading = true
        } else {
            binding.lazyLoadBar.visibility = View.INVISIBLE
            isLoading = false
        }
    }

    private fun populateData() {
        viewModel.filterBscnPostsResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    if (page == 1) {
                        binding.loadBar.visibility = View.GONE
                        binding.rvNews.visibility = View.VISIBLE
                    } else {
                        showLazyLoading(false)
                    }
                    result.body?.let {
                        binding.rvNews.adapter?.let { adapter ->
                            when (adapter) {
                                is NewsBscnAdapter -> {
                                    if (page == 1) {
                                        adapter.clearData()
                                        adapter.updateData(it)
                                    } else {
                                        adapter.updateData(it)
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let {
                        if (page == 1) {
                            binding.loadBar.visibility = View.GONE
                        } else {
                            if (it != "400") {
                                makeToast(it)
                                page.minus(1)
                            } else {
                                makeToast("Last Data")
                            }
                            showLazyLoading(false)
                        }
                    }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })

        viewModel.bscnPostsResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    if (page == 1) {
                        binding.loadBar.visibility = View.GONE
                    } else {
                        showLazyLoading(false)
                    }
                    result.body?.let {
                        binding.rvNews.adapter?.let { adapter ->
                            when (adapter) {
                                is NewsBscnAdapter -> {
                                    adapter.updateData(it)
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let {
                        if (page == 1) {
                            binding.loadBar.visibility = View.GONE
                        } else {
                            if (it != "400") {
                                makeToast(it)
                                page.minus(1)
                            } else {
                                makeToast("Last Data")
                            }
                            showLazyLoading(false)
                        }
                    }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })

        viewModel.authorResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.loadBarFilter.visibility = View.GONE
                        binding.rvFilter.adapter?.let { adapter ->
                            when (adapter) {
                                is FilterAdapter -> {
                                    adapter.updateData(it)
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    binding.loadBarFilter.visibility = View.GONE
                    result.message?.let { makeToast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }

    private fun populateFilterBscnPosts(author: Int, page: Int) {
        if (page == 1) {
            binding.rvNews.visibility = View.GONE
            binding.loadBar.visibility = View.VISIBLE
        } else {
            showLazyLoading(true)
        }
        viewModel.getFilterBscnPosts(author, categories, page)
    }

    private fun populateBscnPosts(categories: String, page: Int) {
        if (page == 1) {
            binding.loadBar.visibility = View.VISIBLE
        } else {
            showLazyLoading(true)
        }
        viewModel.getBscnPosts(categories, page)
    }

}