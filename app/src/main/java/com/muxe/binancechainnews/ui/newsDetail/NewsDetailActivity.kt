package com.muxe.binancechainnews.ui.newsDetail

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.Spannable
import android.text.method.LinkMovementMethod
import android.text.style.ImageSpan
import android.text.style.QuoteSpan
import android.text.style.URLSpan
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import com.bumptech.glide.Glide
import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.data.local.entity.Post
import com.muxe.binancechainnews.databinding.ActivityNewsDetailBinding
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.utils.*
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class NewsDetailActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityNewsDetailBinding
    private lateinit var posts: Posts
    private lateinit var post: Post
    private var isFavorite: Boolean = false
    private val viewModel: NewsDetailViewModel by viewModels()
    private var id: Int? = null

    companion object {
        const val DATA = "data_news"
        const val TYPE_NEWS = "type_news"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewsDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "Detail News"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.fabFavorite.setOnClickListener(this)

        try {
            if (intent.extras?.getString(TYPE_NEWS) == "news_latest") {
                posts = intent.extras?.getParcelable(DATA)!!
                if (!posts.link.isNullOrEmpty()) {
                    id = posts.id
                    binding.tvTitle.text = posts.title?.rendered
                    binding.tvDate.text = ValueFormat.formatDate(posts.date)
                    binding.tvAuthor.text = posts.embedded?.author?.get(0)?.name

                    val findedAvatarDummy = DataDummy.generateDataAvatarDummy().find {
                        it.id == posts.embedded?.author?.get(0)?.id
                    }
                    if (findedAvatarDummy != null) {
                        Glide.with(this)
                                .load(findedAvatarDummy.imgUrl)
                                .circleCrop()
                                .into(binding.imageView)
                    } else {
                        Glide.with(this)
                                .load(posts.embedded?.author?.get(0)?.avatarUrls?.jsonMember96)
                                .circleCrop()
                                .into(binding.imageView)
                    }
                    Glide.with(this)
                            .load(posts.embedded?.wpFeaturedmedia?.get(0)?.link)
                            .centerCrop()
                            .into(binding.imgContent)
                     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                         posts.content?.rendered?.let { displayHtml(it) }
                    } else {
                        Html.fromHtml(posts.content?.rendered)
                    }
                }
            } else {
                post = intent.extras?.getParcelable(DATA)!!
                if (!post.link.isNullOrEmpty()) {
                    id = post.id
                    binding.tvTitle.text = post.title
                    binding.tvDate.text = ValueFormat.formatDate(post.date)
                    binding.tvAuthor.text = post.nameAuthor
                    val findedAvatarDummy = DataDummy.generateDataAvatarDummy().find {
                        it.id == post.id
                    }
                    if (findedAvatarDummy?.imgUrl != null) {
                        showImageCircleCrop(findedAvatarDummy.imgUrl, binding.imageView)
                    } else {
                        if (post.avatarUrl != null) {
                            showImageCircleCrop(post.avatarUrl!!, binding.imageView)
                        }
                    }
                    post.linkMedia?.let { showImageCenterCrop(it, binding.imgContent) }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        post.content?.let { displayHtml(it) }
                    } else {
                        Html.fromHtml(post.content)
                    }
                }
            }
            observeDetailPostFavorite()
        } catch (e: IOException) {
            makeToast("Error Load Data")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun displayHtml(html: String) {

        val imageGetter = ImageGetter(resources, binding.tvContent)
        val styledText = HtmlCompat.fromHtml(
            html,    //Instead of copying pasting, I kept it as a string
            HtmlCompat.FROM_HTML_MODE_LEGACY,
            imageGetter,
            null
        )
        ImageClick(styledText as Spannable)
        replaceQuoteSpans(styledText )

        binding.tvContent.text = styledText
        binding.tvContent.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun replaceQuoteSpans(spannable: Spannable) {

        val quoteSpans: Array<QuoteSpan> =
            spannable.getSpans(0, spannable.length - 1, QuoteSpan::class.java)

        for (quoteSpan in quoteSpans) {
            val start: Int = spannable.getSpanStart(quoteSpan)
            val end: Int = spannable.getSpanEnd(quoteSpan)
            val flags: Int = spannable.getSpanFlags(quoteSpan)
            spannable.removeSpan(quoteSpan)
            spannable.setSpan(
                QuoteSpanClass(
                    ContextCompat.getColor(this, R.color.white),
                    ContextCompat.getColor(this, R.color.yellow),
                    5F,
                    50F
                ),
                start,
                end,
                flags
            )
        }

    }

    private fun observeDetailPostFavorite() {
        id?.let {
            viewModel.getPostIsFavorite(it).observe(this, { result ->
                if (result != null) {
                    setFavoriteState(true)
                } else {
                    setFavoriteState(false)
                }
            })
        }
    }

    private fun setFavoriteState(status: Boolean) {
        if (status) {
            isFavorite = true
            binding.fabFavorite.setImageResource(R.drawable.ic_baseline_favorite_24)
        } else {
            isFavorite = false
            binding.fabFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24)
        }
    }

    private fun setFavorite(posts: Posts) {
        val post = Post(posts.id, posts.title?.rendered, posts.link,
            posts.date, posts.embedded?.wpFeaturedmedia?.get(0)?.link, posts.content?.rendered,
            posts.embedded?.author?.get(0)?.id, posts.embedded?.author?.get(0)?.name,
                posts.embedded?.author?.get(0)?.avatarUrls?.jsonMember96)
        if (!isFavorite){
            makeToast("Added favorite")
            viewModel.setFavoritePost(post)
        } else {
            makeToast("Remove favorite")
            viewModel.deleteFavoritePost(post)
        }
    }

    private fun setFavoriteDetail(post: Post) {
        if (!isFavorite){
            makeToast("Added favorite")
            viewModel.setFavoritePost(post)
        } else {
            makeToast("Remove favorite")
            viewModel.deleteFavoritePost(post)
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.fab_favorite -> {
                if (intent.extras?.getString(TYPE_NEWS) == "news_latest") {
                    setFavorite(posts)
                } else {
                    setFavoriteDetail(post)
                }
            }
        }
    }

    fun ImageClick(html: Spannable) {
        for (span in html.getSpans(0, html.length, ImageSpan::class.java)) {
            val flags = html.getSpanFlags(span)
            val start = html.getSpanStart(span)
            val end = html.getSpanEnd(span)
            html.setSpan(object : URLSpan(span.source) {
                override fun onClick(v: View) {
                    Log.d("Detail News", "onClick: url is ${span.source}")
                }
            }, start, end, flags)
        }
    }
}

