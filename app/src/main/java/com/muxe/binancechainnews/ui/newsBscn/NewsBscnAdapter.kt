package com.muxe.binancechainnews.ui.newsBscn

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.binancechainnews.databinding.ItemBscnNewsBinding
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.utils.DataDummy
import com.muxe.binancechainnews.utils.ValueFormat
import com.muxe.binancechainnews.utils.showImageCenterCrop
import com.muxe.binancechainnews.utils.showImageCircleCrop

class NewsBscnAdapter (private val context: Context, private val postsListItem: ArrayList<Posts>, private val clickListener: (Posts) -> Unit)
    : RecyclerView.Adapter<NewsBscnAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemBscnNewsBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
        )
        return ViewHolder(
                binding
        )
    }

    override fun getItemCount(): Int {
        return postsListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(postsListItem[position])
    }

    inner class ViewHolder(private val binding: ItemBscnNewsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Posts) {
            binding.tvTitle.text = data.title?.rendered
            binding.tvDate.text = ValueFormat.formatDate(data.date)
            binding.tvAuthor.text = data.embedded?.author?.get(0)?.name

            data.embedded?.wpFeaturedmedia?.get(0)?.link?.let { context.showImageCenterCrop(it, binding.imgContent) }

            val findedAvatarDummy = DataDummy.generateDataAvatarDummy().find {
                it.id == data.embedded?.author?.get(0)?.id
            }
            if (findedAvatarDummy != null) {
                context.showImageCircleCrop(findedAvatarDummy.imgUrl, binding.imageView)
            } else {
                data.embedded?.author?.get(0)?.avatarUrls?.jsonMember96.let {
                    if (it != null) {
                        context.showImageCircleCrop(it, binding.imageView)
                    }
                }
            }

            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun clearData() {
        postsListItem.clear()
        notifyDataSetChanged()
    }

    fun updateData(newList: List<Posts>) {
        //postsListItem.clear()
        postsListItem.addAll(newList)
        notifyItemRangeChanged(0, postsListItem.size)
        //notifyDataSetChanged()
    }
}
