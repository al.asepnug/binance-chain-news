package com.muxe.binancechainnews.ui.newsBscn

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.data.repository.NewsRepository
import com.muxe.binancechainnews.entity.Author
import com.muxe.binancechainnews.entity.Posts
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewsBscnViewModel @Inject constructor(
    private val newsRepository: NewsRepository
) : ViewModel(){
    private val _bscnPostsResponse = MutableLiveData<ApiResponse<List<Posts>>>()
    private val _authorResponse = MutableLiveData<ApiResponse<List<Author>>>()
    private val _filterBscnPostsResponse = MutableLiveData<ApiResponse<List<Posts>>>()

    val bscnPostsResponse = _bscnPostsResponse
    val authorResponse = _authorResponse
    val filterBscnPostsResponse = _filterBscnPostsResponse

    fun getBscnPosts(categories: String, page: Int) {
        viewModelScope.launch {
            newsRepository.getBscnPosts(categories, page).collect{
                _bscnPostsResponse.value = it
            }
        }
    }

    fun getAuthor() {
        viewModelScope.launch {
            newsRepository.getAuthor().collect{
                _authorResponse.value = it
            }
        }
    }

    fun getFilterBscnPosts(author: Int, categories: String, page: Int) {
        viewModelScope.launch {
            newsRepository.getFilterBscnPosts(author, categories, page).collect {
                _filterBscnPostsResponse.value = it
            }
        }
    }

}