package com.muxe.binancechainnews.ui.listingCoin

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.databinding.ItemListingCoinBinding
import com.muxe.binancechainnews.entity.Coin
import com.muxe.binancechainnews.utils.ValueFormat.currency
import com.muxe.binancechainnews.utils.ValueFormat.round

class ListingCoinAdapter(private val context: Context, private val coinListItem: ArrayList<Coin>, private val clickListener: (Coin) -> Unit)
    : RecyclerView.Adapter<ListingCoinAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemListingCoinBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
        )
        return ViewHolder(
                binding
        )
    }

    override fun getItemCount(): Int {
        return coinListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(coinListItem[position])
    }

    inner class ViewHolder(private val binding: ItemListingCoinBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Coin) {
            binding.tvTitle.text = data.name
            binding.tvSymbol.text = data.symbol
            binding.tvPrice.text = "$ ${(data.quote?.uSD?.price)?.currency()}"
            binding.tvPercent.text = "${(data.quote?.uSD?.percentChange24h)?.round()} %"
            binding.tvNumber.text = (bindingAdapterPosition.plus(1)).toString()
            if (data.quote?.uSD?.percentChange1h!! < 0.0) {
                binding.tvPercent.setTextColor(ContextCompat.getColor(context, R.color.red))
                binding.imgPercent.setImageResource(R.drawable.ic_baseline_arrow_drop_down_24)
                ImageViewCompat.setImageTintList(binding.imgPercent, ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red)));
            }

            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun updateData(newList: List<Coin>) {
        coinListItem.addAll(newList)
        notifyItemRangeChanged(0, coinListItem.size)
    }
}
