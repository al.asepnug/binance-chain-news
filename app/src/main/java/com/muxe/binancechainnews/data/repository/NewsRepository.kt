package com.muxe.binancechainnews.data.repository

import androidx.lifecycle.LiveData
import com.muxe.binancechainnews.data.NewsSource
import com.muxe.binancechainnews.data.local.LocalDataSource
import com.muxe.binancechainnews.data.local.entity.Notifications
import com.muxe.binancechainnews.data.local.entity.Post
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.data.remote.RemoteDataSource
import com.muxe.binancechainnews.entity.Author
import com.muxe.binancechainnews.entity.ListingCoin
import com.muxe.binancechainnews.entity.Posts
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

class NewsRepository @Inject constructor(
        private val remoteDataSource: RemoteDataSource,
        private val localDataSource: LocalDataSource
) : NewsSource {

    override suspend fun getLatestPosts(page: Int, perPage: Int): Flow<ApiResponse<List<Posts>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getLatestPosts(page, perPage)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getSearchPosts(search: String): Flow<ApiResponse<List<Posts>>> {
        return flow {
            emit(ApiResponse.loading())
            val result =  remoteDataSource.getSearchPosts(search)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getBscnPosts(categories: String, page: Int): Flow<ApiResponse<List<Posts>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getBscnPosts(categories, page)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getAuthor(): Flow<ApiResponse<List<Author>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getAuthor()
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getFilterBscnPosts(author: Int, categories: String, page: Int): Flow<ApiResponse<List<Posts>>>  {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getFilterBscnPosts(author, categories, page)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getListingCoin(start: Int, limit: Int): Flow<ApiResponse<ListingCoin>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getBscGiveAway(start, limit)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override fun insertPostFavorite(post: Post) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.insertPostFavorite(post)
        }
    }

    override fun deletePostFavorite(post: Post) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.deletePostFavorite(post)
        }
    }

    override fun updatePostFavorite(post: Post) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.updatePostFavorite(post)
        }
    }

    override fun getPostsFavorite(): LiveData<List<Post>> = localDataSource.getPostsFavorite()

    override fun getPostIsFavorite(id: Int?): LiveData<Post> = localDataSource.getPostIsFavorite(id)

    override fun insertNotifications(notifications: Notifications) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.insertNotifications(notifications)
        }
    }
}