package com.muxe.binancechainnews.data.local.entity

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "notifications")
data class Notifications(
    @PrimaryKey
    @NonNull
    var id: String,

    var title: String? = null,

    var message: String? = null
) : Parcelable