package com.muxe.binancechainnews.data.remote

class ApiResponse<T>(val status: StatusResponse, val body: T?, val error: Error?, val message: String?) {
    enum class StatusResponse {
        SUCCESS,
        ERROR,
        LOADING
    }
    companion object {
        fun <T> success(body: T?): ApiResponse<T> = ApiResponse(StatusResponse.SUCCESS, body, null, null)

        fun <T> error(msg: String?, error: Error?): ApiResponse<T> = ApiResponse(StatusResponse.ERROR, null, error, msg)

        fun <T> loading(data: T? = null): ApiResponse<T> = ApiResponse(StatusResponse.LOADING, data, null, null)
    }
}