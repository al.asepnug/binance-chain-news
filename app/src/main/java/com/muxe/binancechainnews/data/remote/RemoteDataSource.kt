package com.muxe.binancechainnews.data.remote

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.muxe.binancechainnews.entity.Author
import com.muxe.binancechainnews.entity.ErrorResponse
import com.muxe.binancechainnews.entity.ListingCoin
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.utils.ErrorUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.await
import java.io.IOException
import java.net.SocketTimeoutException
import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val retrofit: Retrofit) {
    suspend fun getLatestPosts(page: Int, perPage: Int): ApiResponse<List<Posts>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getLatestPosts(page, perPage) },
            defaultErrorMessage = "Get Latest Posts"
        )
    }

    suspend fun getSearchPosts(search: String): ApiResponse<List<Posts>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getSearchPosts(search) },
            defaultErrorMessage = "Get Search Posts"
        )
    }

    suspend fun getBscnPosts(categories: String, page: Int): ApiResponse<List<Posts>>{
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getBscnPosts(categories, page) },
            defaultErrorMessage = "Get Bscn Posts"
        )
    }

    suspend fun getAuthor(): ApiResponse<List<Author>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getAuthor() },
            defaultErrorMessage = "Get Author Posts"
        )
    }

    suspend fun getFilterBscnPosts(author: Int, categories: String, page: Int): ApiResponse<List<Posts>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getFilterBscnPosts(author, categories, page) },
            defaultErrorMessage = "Get Filter Bscn Posts"
        )
    }

    suspend fun getBscGiveAway(start: Int, limit: Int): ApiResponse<ListingCoin> {
        val url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=df001ef4-0403-496e-91d5-ed852bd02a6c&convert=USD"
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getBscGiveAway(url, start, limit) },
            defaultErrorMessage = "Get BscGiveAway Posts"
        )
    }

    private suspend fun <T> getResponse(
        request: suspend () -> Response<T>,
        defaultErrorMessage: String
    ): ApiResponse<T> {
        return try {
            val result = request.invoke()
            if (result.isSuccessful) {
                return ApiResponse.success(result.body())
            } else if (result.code() == 500) {
                ApiResponse.error(result.code().toString(), null)
            } else {
                try {
                    val message: ErrorResponse = Gson().fromJson(
                        result.errorBody()?.charStream(),
                        ErrorResponse::class.java
                    )
                    ApiResponse.error( message.error ?: message.message ?: defaultErrorMessage, null)
                } catch (e: Exception) {
                    Log.d("errorparse", result.code().toString())
                    val errorResponse = ErrorUtils.parseError(result, retrofit)
                    ApiResponse.error(errorResponse?.message ?: defaultErrorMessage, errorResponse)
                }
            }
        } catch (se3: SocketTimeoutException) {
            ApiResponse.error("timeout", null)
        } catch (e: Throwable) {
            Log.d("exceptionss", e.toString())
            ApiResponse.error("Error internet conection", null)
        }
    }

}