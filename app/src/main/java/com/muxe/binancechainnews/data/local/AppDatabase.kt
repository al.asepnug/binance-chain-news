package com.muxe.binancechainnews.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.muxe.binancechainnews.data.local.entity.Notifications
import com.muxe.binancechainnews.data.local.entity.Post

@Database(entities = [Post::class, Notifications::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
}