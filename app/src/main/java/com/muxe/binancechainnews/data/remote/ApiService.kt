package com.muxe.binancechainnews.data.remote

import com.muxe.binancechainnews.entity.Author
import com.muxe.binancechainnews.entity.ListingCoin
import com.muxe.binancechainnews.entity.Posts
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import retrofit2.http.Url


interface ApiService {
    @GET("posts?_embed&orderby=date")
    suspend fun getLatestPosts(@Query("page") page: Int,
                       @Query("per_page") perPage: Int) : Response<List<Posts>>

    @GET("posts?_embed")
    suspend fun getSearchPosts(@Query("search") search: String) : Response<List<Posts>>

    @GET("posts?_embed&orderby=date")
    suspend fun getBscnPosts(@Query("categories") categories: String,
                     @Query("page") page: Int) : Response<List<Posts>>

    @GET("users")
    suspend fun getAuthor() : Response<List<Author>>

    @GET("posts?_embed&orderby=date")
    suspend fun getFilterBscnPosts(@Query("author") author: Int,
                           @Query("categories") categories: String,
                           @Query("page") page: Int) : Response<List<Posts>>

    @GET
    suspend fun getBscGiveAway(@Url url: String,
                        @Query("start") start: Int,
                       @Query("limit") limit: Int): Response<ListingCoin>
}