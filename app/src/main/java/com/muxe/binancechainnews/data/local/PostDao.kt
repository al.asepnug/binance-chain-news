package com.muxe.binancechainnews.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.muxe.binancechainnews.data.local.entity.Notifications
import com.muxe.binancechainnews.data.local.entity.Post

@Dao
interface PostDao {
    @Query("SELECT * FROM postsFavorite")
    fun getPostsFavorite() : LiveData<List<Post>>

    @Query("SELECT * FROM postsFavorite WHERE id = :id")
    fun getPostIsFavorite(id: Int?) : LiveData<Post>

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = Post::class)
    fun insertPostFavorite(post: Post)

    @Delete
    fun deletePostFavorite(post: Post)

    @Update(entity = Post::class)
    fun updatePostFavorite(post : Post)

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = Notifications::class)
    fun insertNotifications(notifications: Notifications)
}