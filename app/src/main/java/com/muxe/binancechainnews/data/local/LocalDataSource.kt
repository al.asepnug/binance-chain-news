package com.muxe.binancechainnews.data.local

import androidx.lifecycle.LiveData
import com.muxe.binancechainnews.data.local.entity.Notifications
import com.muxe.binancechainnews.data.local.entity.Post
import javax.inject.Inject

class LocalDataSource @Inject constructor(private val postDao: PostDao) {
    fun insertPostFavorite(post: Post) = postDao.insertPostFavorite(post)

    fun getPostsFavorite(): LiveData<List<Post>> = postDao.getPostsFavorite()

    fun deletePostFavorite(post: Post) = postDao.deletePostFavorite(post)

    fun updatePostFavorite(post: Post) = postDao.updatePostFavorite(post)

    fun getPostIsFavorite(id: Int?) = postDao.getPostIsFavorite(id)

    fun insertNotifications(notifications: Notifications) = postDao.insertNotifications(notifications)
}