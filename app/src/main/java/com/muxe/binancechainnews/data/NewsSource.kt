package com.muxe.binancechainnews.data

import androidx.lifecycle.LiveData
import com.muxe.binancechainnews.data.local.entity.Notifications
import com.muxe.binancechainnews.data.local.entity.Post
import com.muxe.binancechainnews.data.remote.ApiResponse
import com.muxe.binancechainnews.entity.Author
import com.muxe.binancechainnews.entity.ListingCoin
import com.muxe.binancechainnews.entity.Posts
import kotlinx.coroutines.flow.Flow

interface NewsSource {
    suspend fun getLatestPosts(page: Int, perPage: Int): Flow<ApiResponse<List<Posts>>>

    suspend fun getSearchPosts(search: String): Flow<ApiResponse<List<Posts>>>

    suspend fun getBscnPosts(categories: String, page: Int): Flow<ApiResponse<List<Posts>>>

    suspend fun getAuthor(): Flow<ApiResponse<List<Author>>>

    suspend fun getFilterBscnPosts(author: Int, categories: String, page: Int): Flow<ApiResponse<List<Posts>>>

    suspend fun getListingCoin(start: Int, limit: Int): Flow<ApiResponse<ListingCoin>>

    fun insertPostFavorite(post: Post)

    fun deletePostFavorite(post: Post)

    fun updatePostFavorite(post: Post)

    fun getPostsFavorite() : LiveData<List<Post>>

    fun getPostIsFavorite(id: Int?) : LiveData<Post>


    fun insertNotifications(notifications: Notifications)

}