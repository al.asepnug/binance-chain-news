package com.muxe.binancechainnews.data.local.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "postsFavorite")
data class Post(
        @PrimaryKey
        var id: Int? = null,

        var title: String? = null,

        var link: String? = null,

        var date: String? = null,

        var linkMedia: String? = null,

        var content: String? = null,

        var idAuthor: Int? = null,

        var nameAuthor: String? = null,

        var avatarUrl: String? = null
) : Parcelable