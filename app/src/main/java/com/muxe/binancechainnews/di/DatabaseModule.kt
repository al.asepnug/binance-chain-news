package com.muxe.binancechainnews.di

import android.content.Context
import androidx.room.Room
import com.muxe.binancechainnews.data.local.AppDatabase
import com.muxe.binancechainnews.data.local.PostDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "postFavorite.db"
        ).build()
    }

    @Provides
    fun providePostsDao(appDatabase: AppDatabase): PostDao {
        return appDatabase.postDao()
    }
}