package com.muxe.binancechainnews.entity

import com.google.gson.annotations.SerializedName

data class Coin(

        @field:SerializedName("symbol")
        val symbol: String? = null,

        @field:SerializedName("last_updated")
        val lastUpdated: String? = null,

        @field:SerializedName("quote")
        val quote: Quote? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("id")
        val id: Int? = null,

)

data class Quote(

        @field:SerializedName("USD")
        val uSD: USD? = null
)

data class Status(

        @field:SerializedName("error_message")
        val errorMessage: Any? = null,

        @field:SerializedName("elapsed")
        val elapsed: Int? = null,

        @field:SerializedName("total_count")
        val totalCount: Int? = null,

        @field:SerializedName("credit_count")
        val creditCount: Int? = null,

        @field:SerializedName("error_code")
        val errorCode: Int? = null,

        @field:SerializedName("timestamp")
        val timestamp: String? = null
)

data class USD(

        @field:SerializedName("percent_change_30d")
        val percentChange30d: Double? = null,

        @field:SerializedName("percent_change_1h")
        val percentChange1h: Double? = null,

        @field:SerializedName("last_updated")
        val lastUpdated: String? = null,

        @field:SerializedName("percent_change_24h")
        val percentChange24h: Double? = null,

        @field:SerializedName("market_cap")
        val marketCap: Double? = null,

        @field:SerializedName("price")
        val price: Double? = null,

        @field:SerializedName("percent_change_60d")
        val percentChange60d: Double? = null,

        @field:SerializedName("volume_24h")
        val volume24h: Double? = null,

        @field:SerializedName("percent_change_90d")
        val percentChange90d: Double? = null,

        @field:SerializedName("percent_change_7d")
        val percentChange7d: Double? = null
)
