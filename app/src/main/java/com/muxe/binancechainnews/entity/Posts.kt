package com.muxe.binancechainnews.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Posts(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("link")
	val link: String? = null,

	@field:SerializedName("title")
	val title: Title? = null,

	@field:SerializedName("content")
	val content: Content? = null,

	@field:SerializedName("latitude")
	val latitude: String? = null,

	@field:SerializedName("longitude")
	val longitude: String? = null,

	@field:SerializedName("_embedded")
	val embedded: Embedded? = null,

	@field:SerializedName("id")
	val id: Int? = null,
) : Parcelable

@Parcelize
data class WpFeaturedmediaItem(

	@field:SerializedName("link")
	val link: String? = null,
) : Parcelable

@Parcelize
data class Embedded(
    @field:SerializedName("author")
    val author: List<AuthorItem?>? = null,

	@field:SerializedName("wp:featuredmedia")
	val wpFeaturedmedia: List<WpFeaturedmediaItem?>? = null
) : Parcelable

@Parcelize
data class Title(

	@field:SerializedName("rendered")
	val rendered: String? = null
) : Parcelable

@Parcelize
data class Content(

	@field:SerializedName("rendered")
	val rendered: String? = null

) : Parcelable

@Parcelize
data class AuthorItem(

    @field:SerializedName("avatar_urls")
    val avatarUrls: AvatarUrls? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("link")
    val link: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("url")
    val url: String? = null,

    @field:SerializedName("slug")
    val slug: String? = null
) : Parcelable

@Parcelize
data class AvatarUrls(

    @field:SerializedName("24")
    val jsonMember24: String? = null,

    @field:SerializedName("48")
    val jsonMember48: String? = null,

    @field:SerializedName("96")
    val jsonMember96: String? = null
) : Parcelable
