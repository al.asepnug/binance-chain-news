package com.muxe.binancechainnews.entity

import com.google.gson.annotations.SerializedName

data class ListingCoin(

	@field:SerializedName("data")
	val data: List<Coin>? = null,

	@field:SerializedName("status")
	val status: Status? = null
)

