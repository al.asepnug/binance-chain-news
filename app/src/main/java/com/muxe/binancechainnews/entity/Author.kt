package com.muxe.binancechainnews.entity

import com.google.gson.annotations.SerializedName

data class Author(

	@field:SerializedName("avatar_urls")
	val avatarUrls: Avatar? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("link")
	val link: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("slug")
	val slug: String? = null
)

data class Avatar(

	@field:SerializedName("24")
	val jsonMember24: String? = null,

	@field:SerializedName("48")
	val jsonMember48: String? = null,

	@field:SerializedName("96")
	val jsonMember96: String? = null
)
