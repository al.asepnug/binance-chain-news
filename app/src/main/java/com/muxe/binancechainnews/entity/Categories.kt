package com.muxe.binancechainnews.entity

class Categories(
        var id: String,
        var name: String,
        var image: Int,
        var url: String,
        var category: String
)