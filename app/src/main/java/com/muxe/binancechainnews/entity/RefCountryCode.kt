package com.muxe.binancechainnews.entity

import com.google.gson.annotations.SerializedName

data class RefCountryCode(

	@field:SerializedName("sw")
	val sw: Sw? = null,

	@field:SerializedName("ne")
	val ne: Ne? = null
)

data class Sw(

	@field:SerializedName("lon")
	val lon: Double? = null,

	@field:SerializedName("lat")
	val lat: Double? = null
)

data class Ne(

	@field:SerializedName("lon")
	val lon: Double? = null,

	@field:SerializedName("lat")
	val lat: Double? = null
)
