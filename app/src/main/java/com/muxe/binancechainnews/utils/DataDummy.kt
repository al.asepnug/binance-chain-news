package com.muxe.binancechainnews.utils

import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.entity.AvatarDummy
import com.muxe.binancechainnews.entity.Categories

object DataDummy {
    fun generateDataAvatarDummy(): List<AvatarDummy> {
        val listAvatar = ArrayList<AvatarDummy>()

        listAvatar.add(
                AvatarDummy(
                        id = 12,
                        imgUrl = "https://binancechain.news/wp-content/uploads/2021/04/bsc-times-logo.png"
                )
        )

        listAvatar.add(
                AvatarDummy(
                        id = 14,
                        imgUrl = "https://binancechain.news/wp-content/uploads/2021/04/Ixz8x0lv_400x400.jpg"
                )
        )

        listAvatar.add(
                AvatarDummy(
                        id = 15,
                        imgUrl = "https://binancechain.news/wp-content/uploads/2021/04/bscdaily-logo.jpg"
                )
        )
        return listAvatar
    }

    fun generateDataCategoriesDummy(): List<Categories> {
        val listCategories = ArrayList<Categories>()

        listCategories.add(
                Categories(
                        id = "1",
                        name = "All BSC News",
                        image = R.drawable.ic_all_bsc,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "15",
                        name = "BSC AMA's",
                        image = R.drawable.ic_bsc_ama,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "7",
                        name = "Facts & Rumors",
                        image = R.drawable.ic_facts,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "9",
                        name = "Legit or Scam?",
                        image = R.drawable.ic_legit,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "0",
                        name = "Market Cap",
                        image = R.drawable.ic_bsc_ecosystem,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "42",
                        name = "BSC Giveaways",
                        image = R.drawable.ic_giveaway,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "16",
                        name = "BSC Learn",
                        image = R.drawable.ic_learn,
                        url = "",
                        category = ""
                )
        )



        listCategories.add(
                Categories(
                        id = "100",
                        name = "Cryptoclopedia",
                        image = R.drawable.ic_general_crypto,
                        url = "https://binancechain.news/cryptoclopedia-overview/",
                        category = ""
                )
        )

        return listCategories
    }

    fun generateDataToolMenuDummy(): List<Categories> {
        val listCategories = ArrayList<Categories>()

        listCategories.add(
                Categories(
                        id = "14",
                        name = "BSC Explorer",
                        image = R.drawable.ic_giveaway,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "100",
                        name = "Cryptocloped",
                        image = R.drawable.ic_general_crypto,
                        url = "",
                        category = "https://binancechain.news/cryptoclopedia-overview/"
                )
        )
        return listCategories
    }

    fun generateDataNewsMenuDummy(): List<Categories> {
        val listCategories = ArrayList<Categories>()

        listCategories.add(
                Categories(
                        id = "1",
                        name = "BSC News",
                        image = R.drawable.ic_all_bsc,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "41",
                        name = "General Crypto News",
                        image = R.drawable.ic_bsc_ama,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "4",
                        name = "BSC Updates",
                        image = R.drawable.ic_facts,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "6",
                        name = "BSC Projects",
                        image = R.drawable.ic_legit,
                        url = "",
                        category = ""
                )
        )

        listCategories.add(
                Categories(
                        id = "13",
                        name = "BSCN Related",
                        image = R.drawable.ic_bsc_ecosystem,
                        url = "",
                        category = ""
                )
        )

        return listCategories
    }
}