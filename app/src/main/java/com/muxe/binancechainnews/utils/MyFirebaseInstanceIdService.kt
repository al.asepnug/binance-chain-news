package com.muxe.binancechainnews.utils

import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.muxe.binancechainnews.data.local.PostDao
import com.muxe.binancechainnews.data.local.entity.Notifications
import com.muxe.binancechainnews.data.repository.NewsRepository
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

class MyFirebaseInstanceIdService  : FirebaseMessagingService()  {

    val TAG = "PushNotifService"
    lateinit var name: String
    private var broadcaster: LocalBroadcastManager? = null

    override fun onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        handleMessage(remoteMessage)
    }

    private fun handleMessage(remoteMessage: RemoteMessage) {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            val intent = Intent("MyData")
            remoteMessage.notification?.let {

//                try {
//                    newsRepository.insertNotifications(Notifications(remoteMessage.data["id"] ?: "tes", it.title, it.body))
//                } catch (exception: Exception) {
//                    Log.d(TAG, exception.toString())
//                }
                //intent.putExtra("message", it.body)
                remoteMessage.data["action"]?.let { it1 -> Log.d(TAG, it1) }
                intent.putExtra("message", remoteMessage.data["action"])
                intent.putExtra("title", it.title)
                intent.putExtra("id", remoteMessage.data["id"])
                intent.putExtra("action", remoteMessage.data["action"])
                broadcaster?.sendBroadcast(intent)
            }
        }
    }

}