package com.muxe.binancechainnews.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import java.io.IOException
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object ValueFormat {
    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    fun formatDate(date: String?): String? {
        @SuppressLint("SimpleDateFormat") val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val myDate: Date?
        var finalDate: String? = null
        try {
            myDate = dateFormat.parse(date!!)
            val timeFormat = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
            if (myDate != null) {
                finalDate = timeFormat.format(myDate)
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return finalDate
    }

    fun Double.currency(): String {
        val format = DecimalFormat("##,###.00")
        format.isDecimalSeparatorAlwaysShown = false
        return format.format(this).toString()
    }

    fun Double.round(): String {
        val number3digits:Double = Math.round(this * 1000.0) / 1000.0
        val number2digits:Double = Math.round(number3digits * 100.0) / 100.0
        return (Math.round(number2digits * 100.0) / 100.0).toString()
    }

    fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap = Bitmap.createBitmap(
                intrinsicWidth,
                intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }
}