package com.muxe.binancechainnews.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.muxe.binancechainnews.R
import com.muxe.binancechainnews.entity.Posts
import com.muxe.binancechainnews.utils.ValueFormat.formatDate

class CustomInfoWindowMarker(val context: Context) : GoogleMap.InfoWindowAdapter {

    private fun uiUpdate(marker: Marker, view: View) {
        val tvTitle = view.findViewById<TextView>(R.id.tvTitleNews)
        val tvDate = view.findViewById<TextView>(R.id.tvDateNews)
        val imgNews = view.findViewById<ImageView>(R.id.imgNews)

        val data: Posts? = marker.tag as Posts?
        data?.embedded?.wpFeaturedmedia?.get(0)?.link?.let { context.showImageCenterInside(it, imgNews) }

        tvTitle.text = data?.title?.rendered
        tvDate.text = formatDate(data?.date)
    }

    override fun getInfoContents(marker: Marker): View {
        val mInfoView = (context as Activity).layoutInflater.inflate(R.layout.custom_info_window, null)
        mInfoView.layoutParams = RelativeLayout.LayoutParams(600, RelativeLayout.LayoutParams.WRAP_CONTENT)
        uiUpdate(marker, mInfoView)
        return mInfoView
    }

    override fun getInfoWindow(p0: Marker): View? {
        return null
    }
}