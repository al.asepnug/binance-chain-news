package com.muxe.binancechainnews.utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.snackbar.Snackbar

fun Context.makeToast(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun Activity.snackBar(message: CharSequence) = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).show()

fun Activity.snackBarWithAction(message: CharSequence, actionMsg: CharSequence, actionListener: () -> Unit) {
    val snackBar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT)
    snackBar.setAction(actionMsg) {
        actionListener()
    }
    snackBar.show()
}

fun Activity.makeStatusBarTransparent() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility =
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
        }
    } else {
        window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }
}

fun Context.showImageCenterCrop(content: Any, view: ImageView) {
    Glide.with(this)
            .load(content)
            .centerCrop()
            .into(view)
}

fun Context.showImageCenterInside(content: Any, view: ImageView) {
    Glide.with(this)
            .load(content)
            .centerInside()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(view)
}

fun Context.showImageCircleCrop(content: Any, view: ImageView) {
    Glide.with(this)
            .load(content)
            .circleCrop()
            .into(view)
}
