package com.muxe.binancechainnews.utils

import com.muxe.binancechainnews.entity.ErrorResponse
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException
import java.lang.Error

object ErrorUtils {
    fun parseError(response: Response<*>, retrofit: Retrofit): Error? {
        val converter = retrofit.responseBodyConverter<Error>(ErrorResponse::class.java, arrayOfNulls(0))
        return try {
            converter.convert(response.errorBody()!!)
        } catch (e: IOException) {
            Error()
        }
    }
}